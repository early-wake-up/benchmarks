/* +++Date last modified: 05-Jul-1997 */

/*
**  BITCNTS.C - Test program for bit counting functions
**
**  public domain by Bob Stout & Auke Reitsma
*/
#include "bitops.h"
#include "platform.h"
#include "mementos_fram.h"
#include "uart.h"
#include <float.h>                              /* For DBL_MAX           */
#include "checkpoint.h"

#define FUNCS  7
#define CDECL /**/

static int CDECL bit_shifter(long int x);

int benchmark() {
  unsigned i, j, n, seed;
  unsigned iterations=1000;

  // 226ms
  for (j = n = 0, seed = 42; j < iterations; j++, seed += 13) {
	  n += bit_count(seed);
    #if defined(SCHEMATIC_PLACEMENT_WC_1E5)
      checkpoint(0xDEAD);
    #endif
  }

  // printf("Optimized 1 bit/loop counter > Bits: %d\n", n);
  #ifdef MANUAL_PLACEMENT
  checkpoint(2300);
  #endif

  // 425 ms
  for (j = n = 0, seed = 42; j < iterations; j++, seed += 13) {
    n += bitcount(seed);
  }

  // printf("Ratko's mystery algorithm > Bits: %d\n", n);
  #ifdef MANUAL_PLACEMENT
  checkpoint(2450);
  #endif

  // 825 ms
  // for (j = n = 0, seed = 42; j < iterations; j++, seed += 13) {
  //   n += ntbl_bitcnt(seed);
  //   #if defined(SCHEMATIC_PLACEMENT_WC_1E5)
  //     checkpoint(0xDEAD);
  //   #endif
  // }

  // printf("Non-recursive bit count by nybbles > Bits: %d\n", n);
  #ifdef MANUAL_PLACEMENT
  checkpoint(3150);
  #endif

  // 77 ms
  for (j = n = 0, seed = 42; j < iterations; j++, seed += 13)
    n += BW_btbl_bitcount(seed);
  // printf("Non-recursive bit count by bytes (BW) > Bits: %d\n", n);
  #ifdef MANUAL_PLACEMENT
  checkpoint(2050);
  #endif
  #if defined(SCHEMATIC_PLACEMENT_WC_1E6) || defined(SCHEMATIC_PLACEMENT_WC_1E5)
    checkpoint(0xDEAD);
  #endif
  // 112 ms
  for (j = n = 0, seed = 42; j < iterations; j++, seed += 13)
    n += AR_btbl_bitcount(seed);
  // printf("Non-recursive bit count by bytes (AR) > Bits: %d\n", n);
  #ifdef MANUAL_PLACEMENT
  checkpoint(2100);
  #endif

  // 544 ms
  for (j = n = 0, seed = 42; j < iterations; j++, seed += 13) {
    n += bit_shifter(seed);
    #if defined(SCHEMATIC_PLACEMENT_WC_1E6) || defined(SCHEMATIC_PLACEMENT_WC_1E5)
    checkpoint(0xDEAD);
    #endif
  }

  // printf("Shift and count bits > Bits: %d\n", n);
  #ifdef MANUAL_PLACEMENT
  checkpoint(2800);
  #endif

  // Removed as we save after each benchmark execution
  // #if defined(SCHEMATIC_PLACEMENT_WC_1E6) || defined(SCHEMATIC_PLACEMENT_WC_1E5)
  //   checkpoint(0xDEAD);
  // #endif
  return n;
}

static int CDECL bit_shifter(long int x)
{
  int i, n;

  for (i = n = 0; x && (i < (sizeof(long) * CHAR_BIT)); ++i, x >>= 1)
    n += (int)(x & 1L);
  return n;
}
