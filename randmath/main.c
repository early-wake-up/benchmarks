/* NIST Secure Hash Algorithm */
#include "msp430.h"
#include "uart.h"
#include "platform.h"
#include "mementos.h"
#include "adc.h"

#define TIMER_INIT 0xF800
#define START_EXPERIMENT BIT5
#define MEASURE BIT4
#define EXPERIMENT_RUNNING BIT7

// #define printf(STR) ;

// BUTTON VERSION
void wait_for_button_press()
{
  // Set P1.1 as INPUT
  P1DIR &= ~BIT1;
  P1OUT |= BIT1;
  P1REN |= BIT1;
  // uart_puts("Ready to execute, press button to start\n");
  // Generate interrupt on a low to high transition
  P1IV |= BIT1;
  P1IES |= BIT1;
  P1IE |= BIT1;
  P1IFG = 0;
  __bis_SR_register(LPM3_bits | GIE);
  P1OUT |= 0;
  P1IES = 0;
  P1IFG = 0;
}

void wait_for_input()
{
  // Set P1.5 as INPUT
  P1DIR &= ~START_EXPERIMENT;
  P1OUT &= ~START_EXPERIMENT;
  // If the Start experiment pin is already up, return
  if (P1IN & START_EXPERIMENT)
  {
    return;
  }
  // uart_puts("Ready to execute, press button to start\n");
  // Generate interrupt on a low to high transition
  P1IV |= START_EXPERIMENT;
  P1IES &= ~START_EXPERIMENT;
  P1IE |= START_EXPERIMENT;
  P1IFG = 0;
  __bis_SR_register(LPM3_bits | GIE);
  P1OUT |= 0;
  P1IES = 0;
  P1IFG = 0;
}

unsigned int abcmath(void);

unsigned int load()
{
  return abcmath();
}

void hexstring(unsigned int d)
{
  unsigned int rb;
  unsigned int rc;

  rb = 32;
  while (1)
  {
    rb -= 4;
    rc = (d >> rb) & 0xF;
    if (rc > 9)
      rc += 0x37;
    else
      rc += 0x30;
    uart_putchar(rc);
    if (rb == 0)
      break;
  }
  uart_putchar(0x0D);
  uart_putchar(0x0A);
}

// Define first boot as a non-volatile variable
int main(void)
{
  platform_init();
  disable_io();
  adc_init();
  P1DIR |= EXPERIMENT_RUNNING;

  int res = mementos_boot();
  if (res)
  {
    printf("Mementos boot failed\n");
    return 1;
  }
  uart_setup();
  printf("Starting experiment\n");
  P1OUT |= EXPERIMENT_RUNNING;
  while (1)
  {
    printf("Waiting for button press\n");
    wait_for_input();
    P1OUT |= EXPERIMENT_RUNNING;
    sleep_until_voltage_reached(3200);
    printf("Button pressed !\n");
    P1OUT &= ~EXPERIMENT_RUNNING;

    unsigned retval;
    for (int i = 0; i < 100; i++)
    {
      retval = load();
    }

    hexstring(retval);

    P1OUT |= EXPERIMENT_RUNNING;
  }

  //

  return 0;
}

// GPIO interrupt PORT1
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = PORT1_VECTOR
_interrupt void PORT1_ISR(void)
#elif defined(__GNUC__)
void __attribute__((interrupt(PORT1_VECTOR))) PORT1_ISR(void)
#else
#error Compiler not supported!
#endif
{
  P1IFG = 0;
  __bic_SR_register_on_exit(LPM3_bits | GIE);
}
