// ****** COMMON MAIN FILE FOR ALL EXPERIMENTS ****** //
// Setup the platform and run the function "load"
// - Handle synchronization mechanism between the monitor and the MSP430
// - Generate signals to inform the monitor of the current state of the MSP430

#include "msp430.h"
#include "uart.h"
#include "platform.h"
#include "adc.h"
#include "checkpoint.h"

// Uncomment to manually trigger the experiment using the MSP430 button (linked to P1.1)
// #define MANUAL

// Uncomment to activate UART debug messages. By default, UART is deactivate to save energy
// #define VERBOSE

#ifdef MANUAL
#warning "Manual mode activated"
#endif

#ifdef VERBOSE
#warning "Verbose mode activated, make sure the MSP is connected to a computer, as communication are blocking"
#endif

/***
| Pin name        | Arduino pin | MSP430 pin |
| --------------- | ----------- | ---------- |
| PIN_START       | 6           | P1.5       |
| PIN_CHKPT       | 5           | P1.3       |
| PIN_SLEEP       | 4           | P1.4       |
| PIN_MSPREADY    | 3           | P1.7       |
| BENCHCOMPLETED  | 2           | P1.6       |
| PIN_REEXECUTION | 7           | P1.2       |
| PIN_STOP        | 8           | P3.0       |
*/
#define MAX_CHECKPOINTS 20
// Note: the following pins are on the port 1 of the MSP430
// Pin used to send the signal to the MSP430 to start the experiment
#define START BIT5

// Pin used to send the signal to the MSP430 to stop the experiment
#define PIN_STOP BIT0
// Pin used to signal when the experiment is running
// It is active low, so it is set to 1 when the experiment is not running
#define MSPREADY BIT7
// Pin used to signal the completion of a benchmark
#define BENCHCOMPLETED BIT6

#define REEXECUTION BIT2

// PORT 2.6
#define EXECUTION BIT6
#define RESTORE BIT5

#ifndef VERBOSE
#define printf(STR) ;
#endif

void load();

volatile int __attribute__((section(".nv"))) nb_execution = 0;
volatile int __attribute__((section(".nv"))) benchmark_end_signaled = 0;
unsigned int __attribute__((section(".data"))) checkpoint_addresses[MAX_CHECKPOINTS];

/*** Update the wake-up voltage of checkpoints, incrementing it by delta
 *
 * delta is in mV
 */
void update_checkpoint_voltage(int delta) {
  for(int i=0; i<MAX_CHECKPOINTS; i++) {
    if(checkpoint_addresses[i] == 0) {
      break;
    }
    // The wake-up voltage is set at the instruction preceding the checkpoint address
    // ADDR-4:	3c 40 52 07 	mov	#1874,	r12 ; Move the wake-up voltage (here 1874mV) into R12
    // ADDR  :  b0 12 86 52 	call	#21126 ; Call checkpoint with argument in R12
    unsigned int addr = checkpoint_addresses[i] - 4;

    // Check that the mov #VOLTAGE, r12 is where it is expected
    unsigned int value = * (unsigned int *)addr;
    if(value != 0x403c) {
      // Pattern not found, skipping to next checkpoint !
      continue;
    }

    // The wake-up voltage is stored with the mov instruction
    addr = checkpoint_addresses[i] - 2;
    unsigned int old_voltage = * (unsigned int *)addr;
    unsigned int new_voltage = old_voltage + delta;
    // TODO check if new voltage is not above max supply voltage ?
    * (unsigned int *) addr = new_voltage;
  }
}

void _checkpoint(int value)
{
#ifdef ADAPTATIVE
    __mementos_checkpoint(value);
#elif defined(ENERGYAWARE)
    __mementos_checkpoint(MAX_ENERGY);
#elif defined(FULL_BUFFER)
    __mementos_checkpoint(VFULL);
#else
    __mementos_checkpoint(0xDEAD);
#endif
}

void checkpoint(int value) {
        nb_execution = 0;
        P2OUT &= ~EXECUTION;
        P1OUT &= ~REEXECUTION;
        _checkpoint(value);
        nb_execution++;
        if(nb_execution > 1){
          P1OUT |= REEXECUTION;
        } else {
          P2OUT |= EXECUTION;
        }
}

// BUTTON VERSION
void wait_for_button_press()
{
  // Set P1.1 as INPUT
  P1DIR &= ~BIT1;
  P1OUT |= BIT1;
  P1REN |= BIT1;
  // uart_puts("Ready to execute, press button to start\n");
  // Generate interrupt on a low to high transition
  P1IV |= BIT1;
  P1IES |= BIT1;
  P1IE |= BIT1;
  P1IFG = 0;
  __bis_SR_register(LPM3_bits | GIE);
  P1OUT |= 0;
  P1IES = 0;
  P1IFG = 0;
}

void wait_for_input()
{
  // Set P1.5 as INPUT
  P1DIR &= ~START;
  P1OUT &= ~START;
  // If the Start experiment pin is already up, return
  if (P1IN & START)
  {
    return;
  }
  // uart_puts("Ready to execute, press button to start\n");
  // Generate interrupt on a low to high transition
  P1IV |= START;
  P1IES &= ~START;
  P1IE |= START;
  P1IFG = 0;
  __bis_SR_register(LPM3_bits | GIE);
  P1OUT |= 0;
  P1IES = 0;
  P1IFG = 0;
}

// Define first boot as a non-volatile variable
int main(void)
{
  platform_init();
  disable_io();
  P1OUT = 0;
  P3OUT = 0;
  P2OUT = 0;
  P1DIR |= MSPREADY + BENCHCOMPLETED + REEXECUTION + MEASURE;
  P2DIR |= EXECUTION;
  P3DIR &= ~PIN_STOP;

  #ifdef VERBOSE
  uart_setup();
  printf("Rebooting ?\n");
  #endif

  int res = mementos_boot();
  if (res)
  {
    printf("Mementos boot failed\n");
    return 1;
  }

  #ifdef VERBOSE
  uart_setup();
  printf("Starting experiment\n");
  #endif



  P1OUT |= MSPREADY;
  while (1)
  {
    #ifdef VERBOSE
    printf("Waiting for button press\n");
    #endif
#ifdef MANUAL
    wait_for_button_press();
#else
    wait_for_input();
#endif
    P1OUT |= MSPREADY;
    sleep_until_voltage_reached(3200);
    #ifdef VERBOSE
    printf("Button pressed, waiting for release !\n");
    #endif
    P1OUT &= ~MSPREADY;

    #ifdef VERBOSE
    printf("Button released !\n");
    #endif

    load();


    P1OUT |= MSPREADY;
  }

  return 0;
}

void benchmark();

void load() {
  int i = 0;
  P2OUT |= EXECUTION;
  do {
    benchmark();
    #ifdef VERBOSE
    printf("\t%d\n", i);
    i++;
    #endif
    // Only signal the end of the benchmark if we are not currently re-executing
    // We might miss some case were the execution does not reach
    if(benchmark_end_signaled != 1) {
      P1OUT |= BENCHCOMPLETED;
      benchmark_end_signaled = 1;
    }
    checkpoint(0xDEAD);
    P1OUT &= ~BENCHCOMPLETED;
    benchmark_end_signaled = 0;
  } while((P3IN & PIN_STOP) == 0) ;

  checkpoint(0xDEAD);
  nb_execution = 0; // Allow for reexecution here, as we are out of the benchmark
  P2OUT &= 0;
  #ifdef VERBOSE
  printf("Received signal, aborting !\n");
  #endif

  // checkpoint(3400);
  P1OUT = MSPREADY; // Remove all other pending signals
}

// GPIO interrupt PORT1
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = PORT1_VECTOR
_interrupt void PORT1_ISR(void)
#elif defined(__GNUC__)
void __attribute__((interrupt(PORT1_VECTOR))) PORT1_ISR(void)
#else
#error Compiler not supported!
#endif
{
  P1IFG = 0;
  __bic_SR_register_on_exit(LPM3_bits | GIE);
}
