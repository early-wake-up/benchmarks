GCC=msp430-elf-gcc
LD=msp430-elf-gcc
TARGET=main.elf
LIB_REL_PATH=../msp430-libutils/
LIBS=$(LIB_REL_PATH)libsp430utils.a
LDSCRIPT = ../msp430fr5969.ld

# Checkpoint placement
PLACEMENT?=-DSCHEMATIC_PLACEMENT_WC_1E5 -DSCHEMATIC_PLACEMENT_WC_1E6
# Compilation options
# Possible options
# -DMANUAL_PLACEMENT
# -DSCHEMATIC_PLACEMENT_WC_1E5
# -DSCHEMATIC_PLACEMENT_WC_1E6

LIBFLAGS?=

COMMONFLAGS = -mmcu=msp430fr5969 #both CFLAGS and LDFLAGS.
CFLAGS = -Og -g3 -gdwarf-2 -ggdb -I/opt/ti/msp430-gcc/include --freestanding $(DEFINES) $(COMMONFLAGS) -DADAPTATIVE $(PLACEMENT)
LDFLAGS = -Xlinker -Map=$(TARGET).map -T$(LDSCRIPT) $(COMMONFLAGS) -L$(LIB_REL_PATH) -lmsp430utils
# Link options
INCLUDES= -I$(LIB_REL_PATH) -I../

$(TARGET): $(OBJS) $(LIBS) main.o
	$(LD) -mmcu=msp430fr5969 -o main.elf $(OBJS) main.o $(LDFLAGS)

main.o: ../main.c
	$(GCC) -mmcu=msp430fr5969 $(INCLUDES) $(CFLAGS) -c $< -o main.o

%.o: %.c
	$(GCC) -mmcu=msp430fr5969 $(INCLUDES) $(CFLAGS) -c $<

${LIBS}:
	echo "Building library with flags: ${LIBFLAGS}"
	make -C ${LIB_REL_PATH} FLAGS="${LIBFLAGS}"

flash: $(TARGET)
	DSLite load -c ../DSLite/MSP-EXP430FR5969LP.ccxml $<

clean:
	rm -f *.o *.elf *.pdf res*.xml *.readelf *.objdump
	rm -f program.xml *.dot *.html 1 cplex.log *.xml.before_ml *_load.xml
	rm -f *.map
	rm -rf checkpoint_analysis project
#   checkpoint_energy.csv

clean-all: clean
	make -C ${LIB_REL_PATH} clean
