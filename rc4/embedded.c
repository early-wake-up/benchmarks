#include "uart.h"

void hexstring(unsigned int d)
{
    // unsigned int ra;
    unsigned int rb;
    unsigned int rc;

    rb = 32;
    while (1)
    {
        rb -= 4;
        rc = (d >> rb) & 0xF;
        if (rc > 9)
            rc += 0x37;
        else
            rc += 0x30;
        uart_putchar(rc);
        if (rb == 0)
            break;
#ifdef MEMENTOS_LL
        checkpoint(0xDEAD);
#endif
    }
    uart_putchar(0x0D);
    uart_putchar(0x0A);
}
void hexstrings(unsigned int d)
{
    // unsigned int ra;
    unsigned int rb;
    unsigned int rc;

    rb = 32;
    while (1)
    {
        rb -= 4;
        rc = (d >> rb) & 0xF;
        if (rc > 9)
            rc += 0x37;
        else
            rc += 0x30;
        uart_putchar(rc);
        if (rb == 0)
            break;
#ifdef MEMENTOS_LL
        checkpoint(0xDEAD);
#endif
    }
    uart_putchar(0x20);
}
void hexstringcr(unsigned int d)
{
    // unsigned int ra;
    unsigned int rb;
    unsigned int rc;

    uart_putchar(0x0D);
    rb = 32;
    while (1)
    {
        rb -= 4;
        rc = (d >> rb) & 0xF;
        if (rc > 9)
            rc += 0x37;
        else
            rc += 0x30;
        uart_putchar(rc);
        if (rb == 0)
            break;
#ifdef MEMENTOS_LL
        checkpoint(0xDEAD);
#endif
    }
    uart_putchar(0x20);
}
